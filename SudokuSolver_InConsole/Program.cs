﻿using SudokuSolver_InConsole.Models;
using System;
using System.IO;

namespace SudokuSolver_InConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var directoryPath = Directory.GetCurrentDirectory();
            var fileName = "TextFile.txt";
            var filePath = Path.Combine(directoryPath, $"{fileName}");

            var sudoku = new Sudoku_9by9();
            sudoku.InitDataFromFile(filePath); // InitNumOfEmptyCells;
            sudoku.PrintToConsole();
            Console.ReadKey();
        }
    }
}
