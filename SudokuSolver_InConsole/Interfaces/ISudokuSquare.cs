﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SudokuSolver_InConsole.Interfaces
{
    interface ISudokuSquare<T>
    {
        bool DoesColumnHaveTheValue(T value, int column);
        bool DoesRowHaveTheValue(T value, int row);
        bool DoesTheColumnFullyFilled(int column);
        bool DoesTheRowFullyFilled(int row);
        void SetValueToTheCell(T value, int row, int column);
        T GetValueByCoords(int row, int column);
    }
}
