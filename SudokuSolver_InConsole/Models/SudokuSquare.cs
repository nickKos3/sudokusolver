﻿using SudokuSolver_InConsole.Extensions;
using SudokuSolver_InConsole.Interfaces;

namespace SudokuSolver_InConsole.Models
{
    public abstract class SudokuSquare<T> : ISudokuSquare<T> where T : struct
    {
        public int SquareSize { get; set; }
        protected T[,] SquareCells;

        public int InitNumOfEmptyCells { get; protected set; }

        protected T DefaultValue = default(T);
        protected (int, int) DefaultCoords = (0, 0);

        #region checks
        public bool DoesColumnHaveTheValue(T value, int column)
        {
            for (int i = 0; i < SquareSize; i++)
            {
                if (GenericComparers<T>.IsEquals(SquareCells[i, column], value))
                    return true;
            }
            return false;
        }

        public bool DoesRowHaveTheValue(T value, int row)
        {
            for (int i = 0; i < SquareSize; i++)
            {
                if (GenericComparers<T>.IsEquals(SquareCells[row, i], value))
                    return true;
            }
            return false;
        }

        public virtual bool DoesTheColumnFullyFilled(int column)
        {
            for (int i = 0; i < SquareSize; i++)
            {
                if (GenericComparers<T>.IsEquals(SquareCells[i, column], DefaultValue))
                    return false;
            }
            return true;
        }

        public virtual bool DoesTheRowFullyFilled(int row)
        {
            for (int i = 0; i < SquareSize; i++)
            {
                if (GenericComparers<T>.IsEquals(SquareCells[row, i], DefaultValue))
                    return false;
            }
            return true;
        }
        #endregion

        public virtual T GetValueByCoords(int row, int column) => SquareCells[row, column];

        public virtual void SetValueToTheCell(T value, int row, int column)
        {
            this.SquareCells[row, column] = value;
        }

        public virtual (int, int) GetCoordsOfValueInSquareOrDefaultOnes(T value)
        {
            for (int i = 0; i < SquareSize; i++)
                for (int j = 0; j < SquareSize; j++)
                    if (GenericComparers<T>.IsEquals(SquareCells[i, j], value))
                        return (i, j);
            return DefaultCoords;
        }
    }
}
