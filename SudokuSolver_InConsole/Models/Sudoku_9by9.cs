﻿using SudokuSolver_InConsole.Extensions;
using SudokuSolver_InConsole.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SudokuSolver_InConsole.Models
{
    public class Sudoku_9by9 : Sudoku_9by9<int>
    {
        public Sudoku_9by9() : base(Enumerable.Range(1, 9).ToList()) { }
    }

    public class Sudoku_9by9<T> : ISudokuSquare<T> where T: struct
    {
        //will be private after testing
        public Square_3by3<T>[][] _sudoku;
        protected readonly List<T> _allowedSymbols;

        protected T DefaultValue = default(T);

        public Sudoku_9by9(List<T> allowedSymbols)
        {
            _allowedSymbols = allowedSymbols;
        }

        public void InitData(Square_3by3<T>[][] sudoku)
        {
            _sudoku = new Square_3by3<T>[sudoku.Length][];
            for (int i = 0; i < sudoku.Length; i++)
            {
                _sudoku[i] = new Square_3by3<T>[sudoku[i].Length];
                for (int j = 0; j < sudoku[i].Length; j++)
                    _sudoku[i][j] = sudoku[i][j];
            }
        }

        public void InitDataFromFile(string path, char NoSymbol = ' ', char EmptyCell = '0')
        {
            var content = FileExtensions.GetFileContentOrDefault(path);
            if (string.IsNullOrWhiteSpace(content))
            {
                Console.WriteLine("Error with reading of file.");
            }
            //while (string.IsNullOrWhiteSpace(con))
            string[] lines = content.Split(
                new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.None
            );

            string UnconvertedSudokuError = "Sudoku from file can't be read cause of wrong format.";
            if (lines.Length % 3 != 0)
            {
                Console.WriteLine(UnconvertedSudokuError);
                return;
            }
            _sudoku = new Square_3by3<T>[lines.Length / 3][];

            int rowIterator = 0;
                
            for (int row = 0; row < lines.Length; row++)
            {
                rowIterator = row / 3;
                if (row % 3 == 0)
                {
                    if (lines[row].Length % 3 != 0)
                    {
                        Console.WriteLine(UnconvertedSudokuError);
                        return;
                    }
                    _sudoku[rowIterator] = new Square_3by3<T>[lines[row].Length / 3];
                }
                
                for (int col = 0; col < lines[row].Length; col +=3)
                {
                    int colIterator = col / 3;
                    var substring = lines[row].Substring(col, 3);
                    if (substring.IndexOf(NoSymbol) != -1)
                    {
                        if(!string.IsNullOrWhiteSpace(substring))
                        {
                            Console.WriteLine(UnconvertedSudokuError);
                            return;
                        }
                        else
                        {
                            _sudoku[rowIterator][colIterator] = null;
                            continue;
                        }
                    }
                    if (row % 3 == 0 && col % 3 == 0)
                    {
                        _sudoku[rowIterator][colIterator] = new Square_3by3<T>(_allowedSymbols);
                    }
                    if (_sudoku[rowIterator][colIterator] == null)
                    {
                        Console.WriteLine(UnconvertedSudokuError);
                        return;
                    }
                    for (int i = 0; i<3; i++)
                    {
                        _sudoku[rowIterator][colIterator].SetValueToTheCell(
                            ConvertationToGeneric<T>.GetGenericTypeFromChar(substring[i]),
                            row % 3,
                            col % 3 + i);
                    }
                }
            }
        }

        public void PrintToConsole()
        {
            Console.WriteLine();
            foreach(var el in _sudoku)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int col = 0; col < el.Length; col++)
                    {
                        if(el[col] == null)
                            Console.Write("   ");
                        else
                        {
                            for (int j = 0; j < 3; j++)
                                Console.Write(el[col].GetValueByCoords(i, j));
                        }
                    }
                    Console.WriteLine();
                }
            }
        }

        public bool DoesColumnHaveTheValue(T value, int column)
        {
            foreach(var squares_3By3 in _sudoku)
            {
                if (RangeExtensions.DoesNumberInTheRange(column, 1 - 1, squares_3By3.Length * 3 - 1))
                {
                    if (squares_3By3[column / 3] != null){
                        if (squares_3By3[column / 3].DoesColumnHaveTheValue(value, column % 3))
                            return true;
                    }
                }
            }
            return false;
        }

        public bool DoesRowHaveTheValue(T value, int row)
        {
            if (RangeExtensions.DoesNumberInTheRange(row, 0, _sudoku.Length * 3 - 1))
            {
                foreach (var square_3By3 in _sudoku[row / 3])
                {
                    if (square_3By3 != null)
                    {
                        if (square_3By3.DoesRowHaveTheValue(value, row % 3))
                            return true;
                    }
                }
            }
            return false;
        }

        public bool DoesTheColumnFullyFilled(int column)
        {
            foreach (var squares_3By3 in _sudoku)
            {
                if (RangeExtensions.DoesNumberInTheRange(column, 1 - 1, squares_3By3.Length * 3 - 1))
                {
                    if (squares_3By3[column / 3] != null)
                    {
                        if (!squares_3By3[column / 3].DoesTheColumnFullyFilled(column % 3))
                            return false;
                    }
                }
            }
            return true;
        }

        public bool DoesTheRowFullyFilled(int row)
        {
            if (RangeExtensions.DoesNumberInTheRange(row, 0, _sudoku.Length * 3 - 1))
            {
                foreach (var square_3By3 in _sudoku[row / 3])
                {
                    if (square_3By3 != null)
                    {
                        if (!square_3By3.DoesTheRowFullyFilled(row % 3))
                            return false;
                    }
                }
            }
            return true;
        }

        public T GetValueByCoords(int row, int column)
        {
            var errorMessage = $"Error. Method name: {MethodBase.GetCurrentMethod().Name}. Params: [{row},{column}]. Ranges: row - [1, {_sudoku.Length * 3}], column - [1,{_sudoku[row / 3].Length}].";
            if (!RangeExtensions.DoesNumberInTheRange(row, 1 - 1, _sudoku.Length * 3 - 1) || !RangeExtensions.DoesNumberInTheRange(column, 1 - 1, _sudoku[row / 3].Length * 3 - 1))
            {
                Console.WriteLine(errorMessage);
                return DefaultValue;
            }
            return _sudoku[row / 3][column / 3].GetValueByCoords(row % 3, column % 3);
        }

        public void SetValueToTheCell(T value, int row, int column)
        {
            var errorMessage = $"Error. Method name: {MethodBase.GetCurrentMethod().Name}. Params: val={value},[{row},{column}].";
            if (!RangeExtensions.DoesNumberInTheRange(row, 0, _sudoku.Length * 3 - 1) || !RangeExtensions.DoesNumberInTheRange(column, 1 - 1, _sudoku[row / 3].Length * 3 - 1))
            {
                Console.WriteLine(errorMessage);
                return;
            }
            _sudoku[row / 3][column / 3].SetValueToTheCell(value, row % 3, column % 3);
        }
    }
}
