﻿using SudokuSolver_InConsole.Extensions;
using SudokuSolver_InConsole.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SudokuSolver_InConsole.Models
{
    public class Square_3by3 : Square_3by3<int>
    {
        protected readonly int MinValue = 1;
        protected const int MaxValue = 9;

        public Square_3by3() : base(Enumerable.Range(1, MaxValue).ToList()) {}
    }

    public class Square_3by3<T> : SudokuSquare<T>, ISudokuSquare<T> where T : struct
    {
        protected readonly List<T> _allowedSymbols;

        public Square_3by3(List<T> allowedSymbols)
        {
            SquareSize = 3;
            SquareCells = new T[3, 3];
            _allowedSymbols = allowedSymbols;

            NotFoundNumbersList = new List<T>(_allowedSymbols);
            InitNumOfEmptyCells = NumOfEmptyCells;
        }

        public List<T> NotFoundNumbersList { get; private set; }
        public int NumOfEmptyCells => NotFoundNumbersList.Count;

        public override T GetValueByCoords(int row, int column)
        {
            var errorMessage = $"Error. Method name: {MethodBase.GetCurrentMethod().Name}. Params: [{row},{column}]. Ranges: [1,{SquareSize}].";
            if (!RangeExtensions.DoesNumberInTheRange(row, 1 - 1, SquareSize - 1) || !RangeExtensions.DoesNumberInTheRange(column, 1 - 1, SquareSize - 1))
            {
                Console.WriteLine(errorMessage);
                return DefaultValue;
            }
            return base.GetValueByCoords(row, column);
        }

        public override void SetValueToTheCell(T value, int row, int column)
        {
            if (GenericComparers<T>.IsEquals(value, DefaultValue))
                return;
            var errorMessage = $"Error. Method name: {MethodBase.GetCurrentMethod().Name}. Params: val={value},[{row},{column}]. Existed value = {this.SquareCells[row, column]}.";
            if (!RangeExtensions<T>.DoesNumberInTheRange(value, _allowedSymbols))
            {
                Console.WriteLine(errorMessage);
                return;
            }
            if (!RangeExtensions.DoesNumberInTheRange(row, 1 - 1, SquareSize - 1) || !RangeExtensions.DoesNumberInTheRange(column, 1 - 1, SquareSize - 1))
            {
                Console.WriteLine(errorMessage);
                return;
            }
            if (GenericComparers<T>.IsEquals(this.SquareCells[row, column], value))
                return;
            if (!GenericComparers<T>.IsEquals(this.SquareCells[row, column], DefaultValue))
            {
                Console.WriteLine(errorMessage);
                return;
            }
            else
            {
                base.SetValueToTheCell(value, row, column);
                NotFoundNumbersList.Remove(value);
            }
        }

        public override (int, int) GetCoordsOfValueInSquareOrDefaultOnes(T value)
        {
            if (NumOfEmptyCells == InitNumOfEmptyCells)
                return DefaultCoords;
            return base.GetCoordsOfValueInSquareOrDefaultOnes(value);
        }
    }
}
