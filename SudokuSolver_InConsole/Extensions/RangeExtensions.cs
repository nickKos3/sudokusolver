﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SudokuSolver_InConsole.Extensions
{
    class RangeExtensions: RangeExtensions<int>
    {
        public static bool DoesNumberInTheRange(double num, int first, int last)
        {
            return first <= num && num <= last;
        }
    }

    class RangeExtensions<T> where T : struct
    {
        public static bool DoesNumberInTheRange(T num, List<T> list)
        {
            return list.IndexOf(num) != -1;
        }
    }
}
