﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SudokuSolver_InConsole.Extensions
{
    public class FileExtensions
    {
        public static string GetFileContentOrDefault(string filePath)
        {
            string text = "";
            if (File.Exists(filePath))
            {
                text = File.ReadAllText(filePath);
            }
            return text;
        }
    }
}
