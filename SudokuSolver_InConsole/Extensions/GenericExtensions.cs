﻿using System.Collections.Generic;

namespace SudokuSolver_InConsole.Extensions
{
    class GenericComparers<T>
    {
        public static bool IsEquals(T obj1, T obj2)
        {
            return Comparer<T>.Default.Compare(obj1, obj2) == 0;
        }
    }

    class ConvertationToGeneric<T>
    {
        public static T GetGenericTypeFromChar(char str)
        {
            return (T)System.Convert.ChangeType(str.ToString(), typeof(T));
        }
    }
}
